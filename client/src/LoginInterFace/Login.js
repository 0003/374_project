import React,{Component} from "react";
import { useEffect } from "react";
import "./login.css";
import history from './../history';


function Login() {

    window.onload= function(){
        document.getElementsByClassName("primaryBar")[0].style.display="none";
    }
  
    const handleSubmit =()=>{
        history.push('/Home')
        document.getElementsByClassName("primaryBar")[0].style.display="block";

    }

        return (
            <div className="container">
            <div className="screen">
                <div className="screen__content">
                <h1>GitLab Project Manager</h1>
                <form className="login" onSubmit={handleSubmit}>
                    <div className="login__field">
                    <i className="login__icon fas fa-user"></i>
                    <input id="icon_prefix" type="text" className="login__input" placeholder="User name / Email"/>
                    </div>
                    <div className="login__field">
                    <i className="login__icon fas fa-lock"></i>
                    <input id="icon_prefix" type="Password" className="login__input" placeholder="Password"/>
                    </div>
                    
                    <button className="button login__submit"   >
                    <span className="button__text">Log In Now</span>
                    <i className="button__icon fas fa-chevron-right"></i>
                    </button>				
                </form>
                <div className="social-login">
                    
                </div>
                </div>
                <div className="screen__background">
                <span className="screen__background__shape screen__background__shape4"></span>
                <span className="screen__background__shape screen__background__shape3"></span>		
                <span className="screen__background__shape screen__background__shape2"></span>
                <span className="screen__background__shape screen__background__shape1"></span>
                </div>
            </div>
        </div>
        );

}
export default Login;

